//header handler
(function () {
    var header = document.querySelector(".header");
    window.addEventListener('scroll', function () {
        if (window.pageYOffset > 50) {
            header.classList.add('header_active');
        } else {
            header.classList.remove('header_active');
        }
    });
})();

//-------------------------------------------------------
//Burger button handler
(function () {
    var buttonOpen = document.querySelector("#burger");
    var headerNav = document.querySelector('.header_nav');
    var buttonClose = document.querySelector('#closeBurger');
    const menuLinks = document.querySelectorAll('.header_link');
    buttonOpen.addEventListener('click', function () {
        headerNav.classList.add('header_nav_active');
    });
    buttonClose.addEventListener('click', function () {
        headerNav.classList.remove('header_nav_active');
    });

    if (window.innerWidth <= 767) {
        //pentru a rula prin fiecare element din menuLink sa puie un event la fiecare elemet
        // for (let i = 0; i< menuLinks.length; i++){
        //     menuLinks[i].addEventListener('click', ()=> {
        //         headerNav.classList.remove('header_nav_active');
        //     })
        // }

        //optiunea 2
        menuLinks.forEach((link, index) => {
            link.addEventListener('click', () => {
                headerNav.classList.remove('header_nav_active');
            });
        });
    }
})();

//----------------------------------------------------------
// Scroll to anchors
(function () {
    var smoothScroll = function smoothScroll(targetEl, duration) {
        var headerElHeight = document.querySelector('.header').clientHeight;
        var target = document.querySelector(targetEl);
        var targetPosition = target.getBoundingClientRect().top - headerElHeight;
        var startPosition = window.pageYOffset;
        var startTime = null;

        var ease = function ease(t, b, c, d) {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t + b;
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        };

        var animation = function animation(currentTime) {
            if (startTime === null) startTime = currentTime;
            var timeElapsed = currentTime - startTime;
            var run = ease(timeElapsed, startPosition, targetPosition, duration);
            window.scrollTo(0, run);
            if (timeElapsed < duration) requestAnimationFrame(animation);
        };

        requestAnimationFrame(animation);
    };

    var scrollTo = function scrollTo() {
        var links = document.querySelectorAll('.js-scroll');
        links.forEach(function (each) {
            each.addEventListener('click', function () {
                var currentTarget = this.getAttribute('href');
                smoothScroll(currentTarget, 1000);
            });
        });
    };

    scrollTo();
})();